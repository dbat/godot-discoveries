
#var test = 0
#
#func _get_property_list() -> Array:
#	return [{name = "test", type = TYPE_INT}]

# 
#- name is the property's name, as a String;
#
#- class_name is an empty StringName, unless the property is TYPE_OBJECT and it inherits from a class;
#
#- type is the property's type, as an int (see Variant.Type);
#
#- hint is how the property is meant to be edited (see PropertyHint);
#
#- hint_string depends on the hint (see PropertyHint);
#
#- usage is a combination of PropertyUsageFlags.

#@export var holding_hammer = false:
#	set(value):
#		holding_hammer = value
#		notify_property_list_changed()
#
#var hammer_type = 0
#
#func xx_get_property_list():
#	# By default, `hammer_type` is not visible in the editor.
#	var property_usage = PROPERTY_USAGE_NO_EDITOR
#
#	if holding_hammer:
#		property_usage = PROPERTY_USAGE_DEFAULT
#
#	var properties = []
#	properties.append({
#		"name": "hammer_type",
#		"type": TYPE_INT,
#		"usage": property_usage, # See above assignment.
#		"hint": PROPERTY_HINT_ENUM,
#		"hint_string": "Wooden,Iron,Golden,Enchanted"
#	})
#
#	return properties

@tool
extends Node3D

# Provides condition for one set of properties
var some_feature1: bool = false
# Provides condition for another set of properties
var some_feature2: bool = true

# This will be shown only if some_feature1 is true
var some_texture: Texture = null

# This will be shown only if some_feature2 is true
var some_float: float = 0.0

# This will be shown only if some_feature2 is false
var some_int: int = 0

func _get_property_list():
	var ret: Array = []
	
	ret.append({
		"name": &"Awesome Feature 1", #<--The & indicates a StringName type. Go figure.
		"type": TYPE_BOOL,
		"usage": PROPERTY_USAGE_DEFAULT | PROPERTY_USAGE_CHECKED
	})
	
	# Conditionally show the texture property
	if (some_feature1):
		ret.append({
			"name": &"Some Texture",
			"type": typeof(Resource),               # Texture is a Resource
			"hint": PROPERTY_HINT_RESOURCE_TYPE,    # We tell we want to edit a Resource
			"hint_string": "Texture",               # And explicitly a Texture
		})
	
	ret.append({
		"name": &"Awesome Feature 2",
		"type": TYPE_BOOL,
	})
	
	# Conditionally show either float or integer
	if (some_feature2):
		ret.append({
			"name": &"Some Float",
			"type": TYPE_FLOAT,
		})
	
	else:
		ret.append({
			"name": &"Some Int",
			"type": TYPE_INT,
		})
	
	return ret

func _set(prop_name:StringName, val):
	# Assume the property exists
	var retval: bool = true
	
	match prop_name:
		&"Awesome Feature 1":
			some_feature1 = val
			notify_property_list_changed()
		
		&"Some Texture":
			some_texture = val
		
		&"Awesome Feature 2":
			some_feature2 = val
			notify_property_list_changed()
		
		&"Some Float":
			some_float = val
		
		&"Some Int":
			some_int = val
		
		_:
			# If here, trying to set a property we are not manually dealing with.
			retval = false
	
	return retval


func _get(prop_name:StringName):
	var retval = null
	
	match prop_name:
		&"Awesome Feature 1":
			return some_feature1
		
		&"Some Texture":
			return some_texture
		
		&"Awesome Feature 2":
			return some_feature2
		
		&"Some Float":
			return some_float
		
		&"Some Int":
			return some_int
	
	return retval
